//if else case
function werewolf(nama, peran){
    if (nama) {
        if (peran) {
            peran = peran.toLowerCase()
            if (peran == "penyihir") {
                console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
                console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
            } else if (peran == "guard") {
                console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
                console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
            } else if (peran == "warewolf") {
                console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
                console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!`)
            }
        } else {
            console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
        }
    } else {
        console.log('Nama harus diisi!');
    }
}
//pengujian
werewolf("","");
werewolf("John","");
werewolf("Jane","Penyihir");
werewolf("Jenita","Guard");
werewolf("Junaedi","Warewolf");


//switch case

var hari = 21;
var bulan  = 1;
var tahun = 1945;

function tanggal(hari, bulan, tahun){
    if (hari > 0 && hari <= 31) {
        if (bulan > 0 && bulan <= 12) {
            if (tahun >= 1900 && tahun <= 2200) {
                switch(buttonPushed) {
                    case 1: { console.log(hari + ' Januari ' + tahun); break; }
                    case 2: { console.log(hari + ' Februari ' + tahun); break; }
                    case 3: { console.log(hari + ' Maret ' + tahun); break; }
                    case 4: { console.log(hari + ' April ' + tahun); break; }
                    case 5: { console.log(hari + ' Mei ' + tahun); break; }
                    case 6: { console.log(hari + ' Juni ' + tahun); break; }
                    case 7: { console.log(hari + ' Juli ' + tahun); break; }
                    case 8: { console.log(hari + ' Agustus ' + tahun); break; }
                    case 9: { console.log(hari + ' September ' + tahun); break; }
                    case 10: { console.log(hari + ' Oktober ' + tahun); break; }
                    case 11: { console.log(hari + ' November ' + tahun); break; }
                    case 12: { console.log(hari + ' Desember ' + tahun); break; }
                    default:  { console.log('Tidak ada inputan'); }
                }
            } else {
                console.log('Tahun tidak valid!')
            }
        } else {
            console.log('Bulan tidak valid!')
        }
    } else {
        console.log('Hari tidak valid!')
    }
}
//pengujian
tanggal(20,5,1999);