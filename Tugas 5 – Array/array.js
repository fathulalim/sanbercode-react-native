//No. 1 (Range) 
function range(startNum, finishNum){
    var number = []
    if (startNum<finishNum){
        for (i = startNum; i <= finishNum; i++){
            number.push(i)
        }
    } else if (startNum>finishNum){
        for (i = startNum; i >= finishNum; i--){
            number.push(i)
        }
    } else {
        return "-1"
    }
    return number
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var number2 = []
    if (startNum<finishNum){
        for (i = startNum; i <= finishNum; i += step){
            number2.push(i)
        }
    } else if (startNum>finishNum){
        for (i = startNum; i >= finishNum; i -= step){
            number2.push(i)
        }
    } else {
        return -1
    }
    return number2
} 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//No. 3 (Sum of Range)
function sum(startNum, finishNum, step){
    var number3 = []
    if (startNum){
        if (finishNum){
            if (step){
                if (startNum<finishNum){
                    for (i = startNum; i <= finishNum; i += step){
                        number3.push(i)
                    }
                } else if (startNum>finishNum){
                    for (i = startNum; i >= finishNum; i -= step){
                        number3.push(i)
                    }
                }
            } else{
                if (startNum<finishNum){
                    for (i = startNum; i <= finishNum; i++){
                        number3.push(i)
                    }
                } else if (startNum>finishNum){
                    for (i = startNum; i >= finishNum; i--){
                        number3.push(i)
                    }
                }
            }
        } else {
            return startNum
        }
    } else {
        return 0
    }
    var jumlah = 0
    for (i=0; i<number3.length; i++){
        jumlah += number3[i]
    }
    return jumlah
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//No. 4 (Array Multidimensi)
function dataHandling(arr){
    for (i = 0; i < arr.length; i++){
        console.log('Nomor ID: '+arr[i][0])
        console.log('Nama Lengkap: '+arr[i][1])
        console.log('TTL: '+arr[i][2]+' '+arr[i][3])
        console.log('Hobi: '+arr[i][4])
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)

//No. 5 (Balik Kata)
function balikKata(kata){
    terbalik = []
    for (i=kata.length-1; i>=0; i--){
        terbalik = terbalik + kata[i]
    }
    return terbalik
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//No. 6 (Metode Array)
function dataHandling2(arr){
    arr.splice(1, 1, "Roman Alamsyah Elsharawy")
    arr.splice(2, 1, "Provinsi Bandar Lampung")
    arr.splice(4,1)
    arr.push('Pria', 'SMA Internasional Metro')
    console.log(arr)
    
    tanggal = arr[3]
    arr_tanggal = tanggal.split("/")
    bulan = arr_tanggal[1]
        if (bulan[0]=="0"){
        bulan -= bulan[0]
    }
    switch(bulan) {
        case 1:   { console.log('Januari'); break; }
        case 2:   { console.log('Februari'); break; }
        case 3:   { console.log('Maret'); break; }
        case 4:   { console.log('April'); break; }
        case 5:   { console.log('Mei'); break; }
        case 6:   { console.log('Juni'); break; }
        case 7:   { console.log('Juli'); break; }
        case 8:   { console.log('Agustus'); break; }
        case 9:   { console.log('September'); break; }
        case 10:   { console.log('Oktober'); break; }
        case 11:   { console.log('November'); break; }
        case 12:   { console.log('Desember'); break; }
        default:  { console.log('Angka salah!'); }}
    tanggal_awal = arr_tanggal.slice()

    tanggal_split = arr_tanggal.sort(function (value1, value2){return value2 - value1})
    console.log(tanggal_split)

    tanggal_join = tanggal_awal.join("-")
    console.log(tanggal_join)

    nama = arr[1]
    nama_15 = nama.slice(0,15)
    console.log(nama_15)
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 