//1. looping while
console.log('LOOPING PERTAMA')
var a = 2;
while (a<=20) {
    console.log(`${a} - I Love Coding`);
    a += 2;
}

console.log('LOOPING KEDUA')
var a = 20;
while (a>=2) {
    console.log(`${a} - I Love Coding`);
    a -= 2;
}

//2 Looping menggunakan for
console.log('LOOPING FOR');
for (b = 1; b <= 20; b++){
    if (b%2!==0){
        if (b%3==0){
            console.log(`${b} - I Love Coding`);
        } else{
            console.log(`${b} - Santai`);
        }
    } else if (b%2==0){
        console.log(`${b} - Berkualitas`);
    }
}

//3 Membuat Persegi Panjang #
console.log('PERSEGI PANJANG');
var pagar = '';
for (col = 1; col <=8; col++) {
    pagar += '#';
    }
for (row = 1; row <=4; row++) {
    console.log(pagar);
}
    

//4 Membuat Tangga
console.log('TANGGA');
var pagar1 = '';
for (row = 1; row <=7; row++) {
    for (col = 0; col < row; col++) {
    pagar1 += '#';
    }
    console.log(pagar1)
    pagar1 = '';
}

//5 Membuat Papan Catur
console.log('PAPAN CATUR');
var papan = '';
for (row = 1; row <=7; row++) {
    for (col = 0; col <=7; col++) {
        if (row%2==0) {
            papan += '#';
            papan += ' ';
        } else {
            papan += ' ';
            papan += '#';
        }
    }
    console.log(papan)
    papan = '';
}