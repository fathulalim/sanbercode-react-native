import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeApp from './tugas/Tugas12/App'
import Latihan from './latihan/index'
import Login from './tugas/Tugas13/LoginScreen'
import Register from './tugas/Tugas13/RegisterScreen'
import About from './tugas/Tugas13/AboutScreen'
import Main from './tugas/Tugas14/App';

export default function App() {
  return (
    <View style={styles.container}>
      <Main />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
