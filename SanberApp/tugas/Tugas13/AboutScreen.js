import React, { Component } from 'react';
import { Platform, StyleSheet, View, Text, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/IonIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.judul}>
                        <Text style={styles.judulText}>Tentang Saya</Text>
                    </View>
                    <View style={styles.profile}>
                        <View style={styles.foto}>
                            <Icon name="person-sharp" size={150} style={styles.fotoIcon}></Icon>
                        </View>
                        <Text style={styles.nama}>Maulana Muhammad</Text>
                        <Text style={styles.jabatan}>React Native Developer</Text>
                    </View>
                    <View style={styles.content}>
                        <View style={styles.card}>
                            <Text style={styles.cardTitle}>Portofolio</Text>
                        </View>
                    </View>
                    <View style={{height: 77, backgroundColor: '#FFFF'}}></View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 63
    },
    judul: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    judulText: {
        fontFamily: 'Roboto',
        fontSize: 36,
        fontWeight: 'bold'
    },
    body: {
        flex: 1
    },
    profile: {
        flex: 1,
        alignItems: 'center',
        marginTop: 12,
        borderWidth: 1,
    },
    foto: {
        width: 200,
        height: 200,
        borderRadius: 100,
        backgroundColor: '#EFEFEF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fotoIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#CACACA'
    },
    nama: {
        marginTop: 24,
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold'
    },
    jabatan: {
        marginTop: 8,
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF'
    },
    content: {
        marginTop: 6,
    },
    card: {
        marginTop: 9,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        alignItems: 'center',
        paddingVertical: 3,
    },
    cardTitle: {
        margin: 5,
        fontFamily: 'roboto',
        fontSize: 18
    },
})