import React, { Component } from 'react';
import { Platform, StyleSheet, View, Text, Image, TouchableOpacity, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.logo}>
                        <Image source={require('./images/logo.png')} style={{width: 375, height: 102}} />
                    </View>
                    <View style={styles.content}>
                        <View style={styles.item}>
                            <Text style={styles.title}>Login</Text>
                        </View>
                        <View style={styles.input}>
                            <View style={styles.item}>
                                <View style={styles.inputBar}>
                                    <Text style={styles.inputTitle}>Username / Email</Text>
                                </View>
                                <TextInput style={styles.inputan} />
                            </View>
                            <View style={styles.item}>
                                <View style={styles.inputBar}>
                                    <Text style={styles.inputTitle}>Password</Text>
                                </View>
                                <TextInput style={styles.inputan} />
                            </View>
                        </View>
                        <View style={styles.button}>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.loginButton}>
                                    <Text style={styles.buttonTitle}>Masuk</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.item}>
                                <Text style={styles.atau}>atau</Text>
                            </View>
                            <View style={styles.item}>
                                <TouchableOpacity style={styles.regButton}>
                                    <Text style={styles.buttonTitle}>Register?</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{height: 77, backgroundColor: '#FFFF'}}></View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 63
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 1
    },
    content: {
        flex: 1,
        alignItems: 'center',
    },
    item: {
        marginTop: 16,
    },
    title: {
        fontFamily: 'Roboto',
        fontSize: 24,
        color: '#003366',
        marginTop: 60
    },
    input: {
        marginTop: 40,
    },
    inputBar: {
        alignItems: 'flex-start',
    },
    inputTitle: {
        fontFamily: 'Roboto',
        fontSize: 16
    },
    inputan: {
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#003366',
        width: 294,
        height: 48,
        marginTop: 4,
        fontSize: 24
    },
    button: {
        marginTop: 16,
        alignItems: 'center'
    },
    loginButton: {
        backgroundColor: '#3EC6FF',
        width: 140,
        height: 40,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
    },
    regButton: {
        backgroundColor: '#003366',
        width: 140,
        height: 40,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTitle: {
        fontFamily: 'Roboto',
        color: '#FFFFFF',
        fontSize: 24
    },
    atau: {
        fontFamily: 'Roboto',
        fontSize: 24,
        color: '#3EC6FF'
    },
})