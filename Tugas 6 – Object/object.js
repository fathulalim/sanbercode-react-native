//No. 1 (Array to Object)
function arrayToObject(arr) {
    var people = new Object()
    for (i=0; i<arr.length; i++){
        var new_obj = new Object()
        new_obj.firstName = arr[i][0]
        new_obj.lastName = arr[i][1]
        new_obj.gender = arr[i][2]
        if (arr[i][3] != null){
            var now = new Date()
            var thisYear = now.getFullYear() // 2020 (tahun sekarang)
            if (arr[i][3] <= thisYear){
                age = thisYear-arr[i][3]
                new_obj.age = age
            } else{
                new_obj.age = "Invalid birth year"
            }
        } else {
            new_obj.age = "Invalid birth year"
        }
        people[new_obj.firstName+" "+new_obj.lastName] = new_obj
    }
    return console.log(people)
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""





//No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var sale = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ]
    var harga = []
    for (i=0; i<sale.length;i++){
        harga.push(sale[i][1])
    }
    var shoping = new Object()
    if (memberId){
        shoping.memberId = memberId
        if (money >= 50000){
            shoping.money = money
            var listPurchased = []
            hargaMax = Math.max(harga)
            if (money >= hargaMax){
                for (i=0; i<harga.length; i++){
                    listPurchased.push(sale[i][0])
                    money = money-sale[i][1]
                    harga.sort(function (value1, value2) { return value1 - value2 } )
                    harga.pop()
                }
            }
            shoping.listPurchased = listPurchased
            shoping.changeMoney = money
        }else{
            return "Mohon maaf, uang tidak cukup"
        }
    }else{
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    return shoping
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//3 (Naik Angkot)
